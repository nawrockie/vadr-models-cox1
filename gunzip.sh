#!/bin/bash
for a in cox1; do
    gunzip $a.cm.gz
    gunzip $a.cm.i1f.gz
    gunzip $a.cm.i1i.gz
    gunzip $a.cm.i1m.gz
    gunzip $a.cm.i1p.gz
    for b in 0p1 0p2 0p3 0p4 0p5 0p59 0p7 0p8 0p9 1p0 1p1 1p2; do 
        gunzip $a.$b.pt.hmm.gz
        gunzip $a.$b.pt.hmm.h3f.gz
        gunzip $a.$b.pt.hmm.h3i.gz
        gunzip $a.$b.pt.hmm.h3m.gz
        gunzip $a.$b.pt.hmm.h3p.gz
    done
done
