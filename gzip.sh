#!/bin/bash
for a in cox1; do
    gzip $a.cm
    gzip $a.cm.i1f
    gzip $a.cm.i1i
    gzip $a.cm.i1m
    gzip $a.cm.i1p
    for b in 0p1 0p2 0p3 0p4 0p5 0p59 0p7 0p8 0p9 1p0 1p1 1p2; do 
        gzip $a.$b.pt.hmm
        gzip $a.$b.pt.hmm.h3f
        gzip $a.$b.pt.hmm.h3i
        gzip $a.$b.pt.hmm.h3m
        gzip $a.$b.pt.hmm.h3p
    done
done
