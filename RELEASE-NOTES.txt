Release notes for vadr cox1 models:

https://bitbucket.org/nawrockie/vadr-models-calici

Download from: https://ftp.ncbi.nlm.nih.gov/pub/nawrocki/vadr-models/cox1/

====================================================

1.2-2:   [Jan 2024]
         - adds missing cox1.fa.ssi file necessary for using -r
         - adds -r to recommended command in 00README.txt
         - no changes to the actual models

1.2-1:   [March 2021]:
         - .hmm file name suffix changed to .pt.hmm suffix
         - cox1.fa files were converted to all uppercase.
         - first model set under git revision control on bitbucket
         - no changes to the actual models

1.1-4:   [Oct 2020]: 86 total models built from 8996 total sequences
           (same set of sequences as 1.1-1 and 0.991.2)
         - splits single arthropod model into 18 order specific models
         - splits cnidaria model into 4 class specific models
         - splits porifera model into 3 class specific models
         - splits annelida model into 2 class specific models
         - splits mollusca model into 7 class specific models
         - splits nematoda model into 2 class specific models
         - splits echinodermata model into 2 class specific models
         - splits platyhelminthes model into 4 class specific models
         - adds vertebrate phyla level model
         - keeps all phylum level models, even those for groups with
           more specific models
         - adds .msub and .xsub files for use with new options in
           vadr to substitute certain models for others (e.g. phyla
           level models for more specific models).

Release notes below are for vadr cox1 up until v1.1-1, which were not
under git control in bitbucket, but are available (along with newer
model files) as of this writing on
https://ftp.ncbi.nlm.nih.gov/pub/nawrocki/vadr-models/cox1/
           
1.1-1:   [Apr 2020]: 43 total models built from 8996 total sequences
           (same set as 1.0.4-1 and 0.991.2)
         - updates blast database files to blast db format 5 for use 
           with updated blast version (v2.10.0+); all previous model
           files included blast db format 4 (v.2.9.0+)
         - adds fasta file of consensus sequence for each model
           (cox1.fa)
         - adds cox1.hmm file for hmmer-based protein validation
         - adds alignments used to build CMs and HMMs, for first time
         - NO CHANGES TO MODELS THEMSELVES (cox1.cm and cox1.minfo
           unchanged)


1.0.4-1: [Apr 2020]: 43 total models built from 8996 total sequences
           (same set as 1.0.4-1 and 0.991.2)
         - renames vadr.cox1.{cm,minfo} to cox1.{cm,minfo}
         - replaces 'vertebrata-' with 'vt-' in model names
         - NO CHANGES TO MODELS THEMSELVES besides name changes
           (new cox1.{cm,minfo} are same as old vadr.cox1.{cm,minfo}

0.991.2: [Dec 2019]: 43 total models built from 8996 total sequences

====================================================
contact: eric.nawrocki@nih.gov
