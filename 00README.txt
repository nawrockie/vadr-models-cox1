Jan 2024
vadr-models-cox1-1.2-2
https://bitbucket.org/nawrockie/vadr-models-cox1

---

VADR documentation can be found here:
https://github.com/nawrockie/vadr/blob/master/README.md

See RELEASE-NOTES.txt for details on changes between model versions. 

--- 

Recommended v-annotate.pl command for validating/annotating cox1
sequences using these models:

v-annotate.pl \
--mdir <PATH-TO-THIS-MODEL-DIR> \
--mkey cox1 \
-r --xmaxdel 3 --xmaxins 3 --xlongest \
--alt_pass lowcovrg --alt_fail fsthicnf,fstlocnf --fstlowthr 0.0 \
--fstminnt 5 --nomisc --noprotid \
--xsub <PATH-TO-THIS-MODEL-DIR>/cox1.1.2-1.phy.xsub
<fastafile> <outputdir>

---
contact: eric.nawrocki@nih.gov

